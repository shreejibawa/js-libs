﻿var app = angular.module("myApp", []);

app.directive('googlePlacesAutocomplete', [function() {
	return {
		restrict: 'A',
		scope: {
			onselected: "&",
			placeType: "=",
			placeTypes: "=",
			componentRestrictions: "="
		},
		controller: ['$scope', '$element', '$timeout', '$attrs', function ($scope, $element, $timeout, $attrs) {
			
			var placeType = $scope.placeType ? $scope.placeType : 'address';
			var componentRestrictions = $scope.componentRestrictions ? $scope.componentRestrictions : {country: 'IN'};
			var autocomplete, autocompleteOptions;

			var componentForm = {
				street_number: 'short_name',
				route: 'long_name', // route - street
				locality: 'long_name', // city name
				administrative_area_level_1: 'short_name', // state name,
				administrative_area_level_2: 'short_name', // state name,
				administrative_area_level_3: 'short_name', // state name
				country: 'long_name', // country name
				postal_code: 'short_name' // postal code
			};

			function updateLocationData() {
				var place = autocomplete.getPlace();
				console.log(place);
				var location = {};
				for (var i = 0; i < place.address_components.length; i++) {
					var addressType = place.address_components[i].types[0];
					if (componentForm[addressType]) {
						var val = place.address_components[i][componentForm[addressType]];
						location[addressType] = val;
					}
				}

				// Call the callback function
				$timeout(function () {
					// If we do not get locality consider Admin Aera Level 3 as a locality 
					if(!location.locality && location.administrative_area_level_3) {
						location.locality = location.administrative_area_level_3;
						delete location.administrative_area_level_3;
					}
					if (!location.locality && location.administrative_area_level_2) {
						location.locality = location.administrative_area_level_2;
						delete location.administrative_area_level_2;
					}

					// $scope.onselected({ locationObject: location });
				});

			}
			
			function intAutoComplete() {

				/* Init autocomplete with options 
				 * Find more about types at https://developers.google.com/places/supported_types
				 * Find more about components restrictions at https://developers.google.com/maps/documentation/javascript/3.exp/reference#ComponentRestrictions
				 */

				// prepare options
				autocompleteOptions = {
					componentRestrictions: componentRestrictions
				};
				
				// if ($scope.placeTypes) {
					// autocompleteOptions.types = ['(regions)'];
					autocompleteOptions.type = 'address';
				// } else if ($scope.placeType) {
				// 	autocompleteOptions.type = placeType;
				// }

				// Init Autocomplete
				autocomplete = new google.maps.places.Autocomplete($element[0], autocompleteOptions);

				/* When the user selects an address from the dropdown, populate the address */

				autocomplete.addListener('place_changed', updateLocationData);

				/* Prevent form submit event on press of enter key */
				google.maps.event.addDomListener($element[0], 'keydown', function(e) {
					if (e.keyCode === 13) {
						e.preventDefault();
					}
				});
			}

			intAutoComplete();
		}]
	};
}]);
